package ru.cirreth.sceniclighting

import kotlinx.coroutines.*
import kotlinx.coroutines.test.setMain
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import ru.cirreth.sceniclighting.domain.models.Chase
import ru.cirreth.sceniclighting.domain.models.Scene

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        val a1 = Array(512, init = { 0.toUInt().toUByte() })
        val a2 = Array(512, init = { 0.toUInt().toUByte() })
        a1[0] = 255.toUByte()
        a2[0] = 255.toUByte()
        a1[1] = 0.toUByte()
        a2[1] = 255.toUByte()
        a1[2] = 128.toUByte()
        a2[2] = 255.toUByte()
        a1[3] = 255.toUByte()
        a2[3] = 0.toUByte()
        val chaser = Chaser(
            listOf(
                Scene(10000, 0, a1),
                Scene(10000, 0, a2)
            )
        ) {
            println(it.take(5).joinToString(", "))
        }
        chaser.reset()
        assertEquals(a1[0].toInt(), chaser.state[0].toInt())
        assertEquals(a1[1].toInt(), chaser.state[1].toInt())
        assertEquals(a1[2].toInt(), chaser.state[2].toInt())
        assertEquals(a1[3].toInt(), chaser.state[3].toInt())
        // 10%
        chaser.setTransitionProgress(0.1F)
        assertEquals(255, chaser.state[0].toInt())
        assertEquals(0+26, chaser.state[1].toInt())
        assertEquals(128+13, chaser.state[2].toInt())
        assertEquals(255-26, chaser.state[3].toInt())
        // 50%
        chaser.setTransitionProgress(0.5F)
        assertEquals(255, chaser.state[0].toInt())
        assertEquals(0+128, chaser.state[1].toInt())
        assertEquals(128+64, chaser.state[2].toInt())
        assertEquals(255-128, chaser.state[3].toInt())
        // 99%
        chaser.setTransitionProgress(0.99F)
        assertEquals(255, chaser.state[0].toInt())
        assertEquals(252, chaser.state[1].toInt())
        assertEquals(254, chaser.state[2].toInt())
        assertEquals(3, chaser.state[3].toInt())
        // scene2 ( second to first return )
        chaser.cursorNext()
        // scene starts with scene2 values
        assertEquals(a2[0].toInt(), chaser.state[0].toInt())
        assertEquals(a2[1].toInt(), chaser.state[1].toInt())
        assertEquals(a2[2].toInt(), chaser.state[2].toInt())
        assertEquals(a2[3].toInt(), chaser.state[3].toInt())
        // set 10%
        chaser.setTransitionProgress(0.1F)
        assertEquals(255-26, chaser.state[1].toInt())
        assertEquals(255-13, chaser.state[2].toInt())
        assertEquals(26, chaser.state[3].toInt())
        // set 99%
        chaser.setTransitionProgress(0.99F)
        assertEquals(3, chaser.state[1].toInt())
        assertEquals(129, chaser.state[2].toInt())
        assertEquals(252, chaser.state[3].toInt())
        // scene1 ( first scene, target - 2 )
        chaser.cursorNext()
        // scene starts with scene2 values
        assertEquals(a1[0].toInt(), chaser.state[0].toInt())
        assertEquals(a1[1].toInt(), chaser.state[1].toInt())
        assertEquals(a1[2].toInt(), chaser.state[2].toInt())
        assertEquals(a1[3].toInt(), chaser.state[3].toInt())
    }

}