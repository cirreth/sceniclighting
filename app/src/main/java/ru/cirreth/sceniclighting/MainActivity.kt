package ru.cirreth.sceniclighting

import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import ru.cirreth.sceniclighting.artnet.ArtnetController
import ru.cirreth.sceniclighting.domain.models.Scene
import ru.cirreth.sceniclighting.ui.composable.BottomBar
import ru.cirreth.sceniclighting.ui.composable.RemoteScreen
import ru.cirreth.sceniclighting.ui.composable.SettingsScreen
import ru.cirreth.sceniclighting.ui.theme.ScenicLightingTheme
import ru.cirreth.sceniclighting.ui.vm.RemoteViewModel
import ru.cirreth.sceniclighting.ui.vm.SettingsViewModel

class MainActivity : ComponentActivity(), DIAware {

    override val di by closestDI()
    private val artnetController: ArtnetController by instance()
    private val settingsViewModel: SettingsViewModel by instance()
    private val remoteViewModel: RemoteViewModel by instance()


    val items = listOf(
        BottomMenuItem("Home", Icons.Filled.Home),
        BottomMenuItem("Devices", Icons.Filled.Build),
        BottomMenuItem("Scene", Icons.Filled.Build),
        BottomMenuItem("Remote", Icons.Filled.Menu),
        BottomMenuItem("Settings", Icons.Filled.Settings),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val screen = mutableStateOf(0)
        setContent {
            val navController = rememberNavController()
            ScenicLightingTheme {
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        content = {
                            NavHost(navController = navController, startDestination = "Remote") {
                                composable("Home") { Content() }
                                composable("Devices") { Devices() }
                                composable("Scene") { ArrangementComposable(artnetController) }
                                composable("Remote") { RemoteScreen(remoteViewModel) }
                                composable("Settings") { SettingsScreen(settingsViewModel) }
                            }
                        },
                        bottomBar = { BottomBar(items, screen, navController) }
                    )
                }
            }
        }
        artnetController.connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        artnetController.disconnect()
    }
}


data class BottomMenuItem(
    val name: String,
    val icon: ImageVector
)

@Composable
fun Content() {
    Column {
        Text("HOME")
    }
}

@Composable
fun Devices() {
    Column {
        Text("DEVICES")
    }
}


@Composable
fun ArrangementComposable(artnetController: ArtnetController) {
    var data by remember { mutableStateOf("") }

    val a1 = Array(512, init = { 0.toUInt().toUByte() })
    val a2 = Array(512, init = { 0.toUInt().toUByte() })
    val a3 = Array(512, init = { 0.toUInt().toUByte() })
    val a4 = Array(512, init = { 0.toUInt().toUByte() })
    a1[0] = 255.toUByte()
    a1[1] = 255.toUByte()
    a1[2] = 0.toUByte()
    a1[3] = 0.toUByte()
    a1[4] = 0.toUByte()

    a2[0] = 255.toUByte()
    a2[1] = 0.toUByte()
    a2[2] = 255.toUByte()
    a2[3] = 0.toUByte()
    a2[4] = 0.toUByte()

    a3[0] = 255.toUByte()
    a3[1] = 0.toUByte()
    a3[2] = 0.toUByte()
    a3[3] = 255.toUByte()
    a3[4] = 0.toUByte()

    a4[0] = 255.toUByte()
    a4[1] = 0.toUByte()
    a4[2] = 0.toUByte()
    a4[3] = 0.toUByte()
    a4[4] = 255.toUByte()
    var driver: ChaserDriver? = null

    Column {
        Button(onClick = {
            driver = ChaserDriver(Chaser(
                listOf(
                    Scene(0, 1000, a1),
                    Scene(0, 1000, a2),
                    Scene(1000, 1000, a3),
                    Scene(3000, 1000, a4)
                )
            ) {
                artnetController.set(it)
                data = it.take(5).joinToString(", ")
            })
        }) {
            Text("Start")
        }
        Text(data)
        Button(onClick = {
            driver?.cancel()
        }) {
            Text("Cancel")
        }
    }
}
