package ru.cirreth.sceniclighting

import android.util.Log
import kotlinx.coroutines.*
import ru.cirreth.sceniclighting.domain.models.Scene
import java.time.Duration
import java.time.Instant
import kotlin.math.round

class ChaserDriver(private val chaser: Chaser) {

    private var changeStartTime: Instant? = null
    private var paused: Boolean = false
    private var job: Job

    init {
        job = MainScope().launch {
            reset()
            while (true) {
                while(true) {
                    val s1 = System.currentTimeMillis()
                    if (transition()) break
                    val s2 = System.currentTimeMillis()
                    delay(40-(s2-s1))
                }
                waitSceneTime()
                chaser.cursorNext()
                changeStartTime = Instant.now()
            }
        }
    }

    fun reset() {
        chaser.reset()
        changeStartTime = Instant.now()
    }

    /**
     * @return is transition complete
     */
    private fun transition(): Boolean {
        val now = Instant.now()
        val progress = Duration.between(changeStartTime, now).toMillis() / chaser.nextScene.changeTime.toFloat()
        if (progress >= 1F) {
            return true
        }
        chaser.setTransitionProgress(progress)
        return false
    }

    private suspend fun waitSceneTime() {
        val curScene = chaser.sequence[chaser.cursor]
        delay(curScene.time)
    }

    fun cancel() {
        job.cancel()
    }

}

class Chaser(
    val sequence: List<Scene> = mutableListOf(),
    private val apply: (Array<UByte>) -> Unit
) {


    var state: Array<UByte> = Array(512, init = { 0.toUByte() })
    var cursor: Int = 0
    lateinit var nextScene: Scene

    fun reset() {
        if (sequence.isEmpty()) {
            return
        }
        if (sequence.size == 1) {
            state = sequence.first().values.clone()
            apply.invoke(state)
            return
        }
        cursor = 0
        state = sequence[cursor].values.clone()
        nextScene = sequence[1]
    }

    fun setTransitionProgress(progress: Float) {
        val curScene = sequence[cursor]
        for (i in 0 until 64) {
            if (curScene.values[i] == nextScene.values[i]) {
                state[i] = nextScene.values[i]
            }
            state[i] = if (nextScene.values[i] > curScene.values[i]) {
                (curScene.values[i].toInt() + round((nextScene.values[i] - curScene.values[i]).toFloat() * progress).toInt()).toUByte()
            } else {
                (curScene.values[i].toInt() - round((curScene.values[i] - nextScene.values[i]).toFloat() * progress).toInt()).toUByte()
            }
        }
        apply.invoke(state)
    }

    fun cursorNext() {
        state = nextScene.values.clone()
        if (cursor + 1 < sequence.size) {
            cursor++
        } else {
            cursor = 0
        }
        val nextCursor = if (cursor + 1 < sequence.size) {
            cursor + 1
        } else {
            0
        }
        nextScene = sequence[nextCursor]
    }

}
