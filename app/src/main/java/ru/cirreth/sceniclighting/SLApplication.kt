package ru.cirreth.sceniclighting

import android.app.Application
import android.util.Log
import androidx.room.Room
import com.google.gson.Gson
import org.kodein.di.*
import ru.cirreth.sceniclighting.artnet.ArtnetController
import ru.cirreth.sceniclighting.dev.arrangement
import ru.cirreth.sceniclighting.domain.models.LightingTypeConverter
import ru.cirreth.sceniclighting.repository.DB
import ru.cirreth.sceniclighting.repository.SharedPreferencesStorage
import ru.cirreth.sceniclighting.ui.vm.RemoteViewModel
import ru.cirreth.sceniclighting.ui.vm.SettingsViewModel
import java.util.concurrent.Executors

const val APP_TAG = "SCENICLIGHTING"

class SLApplication: Application(), DIAware {

    override val di by DI.lazy {
        bind<Gson>() with singleton { Gson() }
        bind<LightingTypeConverter>() with singleton { LightingTypeConverter(instance()) }
        bind<ArtnetController>() with singleton { ArtnetController(instance()) }
        bind<SharedPreferencesStorage>() with singleton { SharedPreferencesStorage(this@SLApplication) }
        bind<DB>() with eagerSingleton {
            Room.databaseBuilder(
            this@SLApplication, DB::class.java, "db")
            .fallbackToDestructiveMigration()
            .setQueryCallback({ sqlQuery, bindArgs ->
                Log.d(APP_TAG, "SQL Query: $sqlQuery SQL Args: $bindArgs")
            }, Executors.newSingleThreadExecutor())
            .addTypeConverter(instance() as LightingTypeConverter)
            .build() }
        bind<RemoteViewModel>() with singleton { RemoteViewModel(instance(), arrangement, instance()) }
        bind<SettingsViewModel>() with singleton { SettingsViewModel(instance()) }
    }

}