package ru.cirreth.sceniclighting.ui.composable

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import ru.cirreth.sceniclighting.domain.models.SettingsData
import ru.cirreth.sceniclighting.ui.vm.SettingsViewModel

@Composable
fun SettingsScreen(vm: SettingsViewModel) {

    var address by remember { mutableStateOf(vm.address.value) }

    Column {
        Text("Settings")
        TextField(address, onValueChange = { address = it })
        Button(onClick = { vm.save(SettingsData(address)) }) {
            Text("Save")
        }
    }

}