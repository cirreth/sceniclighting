package ru.cirreth.sceniclighting.ui.vm

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import ru.cirreth.sceniclighting.APP_TAG
import ru.cirreth.sceniclighting.artnet.ArtnetController
import ru.cirreth.sceniclighting.domain.models.*
import ru.cirreth.sceniclighting.repository.DB

class RemoteViewModel(
    private val artnetController: ArtnetController,
    val arrangement: Arrangement,
    val db: DB
) {

    val sequence = mutableListOf<Scene>()
    val selectedSceneIdx = MutableStateFlow(-1)
    val state = artnetController.dmxData
    val selectedDeviceStartChannel = MutableStateFlow(0)
    val selectedDevice = MutableStateFlow(arrangement.devices.values.firstOrNull())
    val scene = selectedSceneIdx.map { sequence.getOrNull(it) }
    val channels = selectedDevice.map { it?.deviceType?.channels}

    init {
        MainScope().launch(Dispatchers.IO) {
            selectedSceneIdx.collectLatest {
                if (it >= 0) {
                    artnetController.set(sequence[it].values)
                }
            }
        }
    }

    fun setChannel(channel: Int, value: Int) {
        if (selectedSceneIdx.value >= 0) {
            sequence[selectedSceneIdx.value].values[channel] = value.toUByte()
        }
        artnetController.send(channel, value)
    }

    fun saveSequence() {
        MainScope().launch(Dispatchers.IO) {
            val chase = Chase(frames = sequence)
            db.chaseDao().insertAll(chase)
        }
    }

    fun reload() {
        MainScope().launch(Dispatchers.IO) {
            db.chaseDao().getAll().collectLatest { chases ->
                chases.last().let { chse ->
                   Log.e(APP_TAG, chse.frames.size.toString())
                   sequence.clear()
                   sequence.addAll(chse.frames)
                   selectedSceneIdx.emit(0)
                }
            }
        }
    }

}