package ru.cirreth.sceniclighting.ui.composable

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Slider
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.cirreth.sceniclighting.APP_TAG
import ru.cirreth.sceniclighting.Chaser
import ru.cirreth.sceniclighting.domain.models.ChannelType
import ru.cirreth.sceniclighting.domain.models.Scene
import ru.cirreth.sceniclighting.ui.vm.RemoteViewModel
import kotlin.math.absoluteValue
import kotlin.math.floor

@Composable
fun RemoteScreen(vm: RemoteViewModel) {

    val selectedDeviceStartChannel by vm.selectedDeviceStartChannel.collectAsState()
    val selectedDevice by vm.selectedDevice.collectAsState()
    var sequence by remember { mutableStateOf(vm.sequence) }
    val selectedSceneIdx by vm.selectedSceneIdx.collectAsState()
    val channels by vm.channels.collectAsState(null)
    val scene by vm.scene.collectAsState(null)

    Column(
        Modifier
            .padding(35.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Text("scenes: ${sequence.size}")
        Text("scene idx: $selectedSceneIdx")
        vm.sequence.forEachIndexed { idx, _ ->
            Button(onClick = {
                vm.selectedSceneIdx.value = idx
            }) {
                Text(idx.toString())
            }
        }
        Button(onClick = {
            vm.sequence.add(Scene(0, Long.MAX_VALUE, vm.state.toTypedArray()))
            sequence = vm.sequence
            vm.selectedSceneIdx.value = vm.sequence.size - 1
        }) {
            Text("Add")
        }
        Button(onClick = {
//            Chaser(sequence) {
//                Log.d(APP_TAG, it.take(5).joinToString(", "))
//            }
            vm.saveSequence()
        }) {
            Text("Save")
        }
        Button(onClick = {
//            Chaser(sequence) {
//                Log.d(APP_TAG, it.take(5).joinToString(", "))
//            }
            vm.reload()
        }) {
            Text("Load")
        }
        TextField(
            value = selectedDeviceStartChannel.toString(),
            onValueChange = { vm.selectedSceneIdx.value = it.toIntOrNull() ?: 0 }
        )
        vm.arrangement.devices.values.forEach { device ->
            Button(onClick = {
                vm.selectedDeviceStartChannel.value = device.startChannel
                vm.selectedDevice.value = device
            }) {
                Text(device.deviceType.name)
            }
        }
        Text("${selectedDevice?.id}: ${selectedDevice?.deviceType?.name}")
        selectedDevice?.deviceType?.presets?.presets?.forEach { preset ->
            Button(onClick = {
                preset.channelSets.forEach { chset ->
                    val channelNumber = (selectedDevice?.startChannel ?: 0) + chset.channel
                    vm.setChannel(channelNumber, chset.value.toInt())
                }
            }) {
                Text(preset.name)
            }
        }
        channels?.channels?.forEachIndexed { idx, ch ->
            val faderChannel = (selectedDevice!!.startChannel) + idx
            val storedValue = (scene?.values?.get(faderChannel)?.toFloat() ?: 0F)
            var value by remember { mutableStateOf(storedValue) }
            Text("${ch.name} (${value.toInt()})")
            Log.e(APP_TAG, "$selectedSceneIdx - ${ch.name} (${value.toInt()})")
            when (ch.type) {
                ChannelType.FADE,
                ChannelType.FADE_REVERSE,
                ChannelType.PAN,
                ChannelType.TILT -> {
                    Slider(
                        value = value,
                        valueRange = ch.minValue.toFloat()..ch.maxValue.toFloat(),
                        onValueChange = {
                            value = floor(it)
                            val faderValue =
                                if (ch.type == ChannelType.FADE_REVERSE) 255 - value.toInt() else value.toInt()
                            vm.setChannel(faderChannel, faderValue)
                        }
                    )
                }
                ChannelType.DISCRETE -> {
                    ch.discreteRanges?.forEach { (range, name) ->
                        Button(onClick = {
                            vm.setChannel(faderChannel, range.first)
                        }) {
                            Text(name)
                        }
                    }
                }
                else -> {
                }
            }
            value = storedValue
        }
    }



}
