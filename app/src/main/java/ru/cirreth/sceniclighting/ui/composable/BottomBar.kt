package ru.cirreth.sceniclighting.ui.composable

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.navigation.NavHostController
import ru.cirreth.sceniclighting.BottomMenuItem


@Composable
fun BottomBar(items: List<BottomMenuItem>, selectedItem: MutableState<Int>, navController: NavHostController) {
    BottomNavigation {
        items.forEachIndexed { index, item ->
            BottomNavigationItem(
                icon = { Icon(item.icon, item.name) },
                label = { Text(item.name) },
                selected = selectedItem.value == index,
                onClick = {
                    selectedItem.value = index
                    navController.navigate(item.name) { launchSingleTop = true }
                },
                alwaysShowLabel = false
            )
        }
    }
}
