package ru.cirreth.sceniclighting.ui.vm

import androidx.lifecycle.ViewModel
import ru.cirreth.sceniclighting.domain.models.SettingsData
import ru.cirreth.sceniclighting.repository.SharedPreferencesStorage

class SettingsViewModel(
    private val sharedPreferencesStorage: SharedPreferencesStorage
): ViewModel() {

    var address = sharedPreferencesStorage.getArtNetAddress()

    fun save(data: SettingsData) {
        sharedPreferencesStorage.saveArtNetAddress(data.address)
    }

}