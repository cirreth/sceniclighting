package ru.cirreth.sceniclighting.repository

import android.content.Context
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SharedPreferencesStorage(private val context: Context) {

    private val sharedPreferencesName = "sceniclighting"
    private val sharedPreferences = context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE)

    private val addressFlow = MutableStateFlow(loadArtNetAddress())

    fun saveArtNetAddress(address: String) {
        sharedPreferences.edit().putString("address", address).commit()
        MainScope().launch {
            addressFlow.emit(address)
        }
    }

    fun getArtNetAddress(): StateFlow<String> = addressFlow.asStateFlow()

    private fun loadArtNetAddress(): String {
        return sharedPreferences.getString("address", null) ?: "192.168.0.100"
    }

}