package ru.cirreth.sceniclighting.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.cirreth.sceniclighting.domain.models.Chase
import ru.cirreth.sceniclighting.domain.models.DeviceType
import ru.cirreth.sceniclighting.domain.models.LightingTypeConverter
import ru.cirreth.sceniclighting.repository.dao.ChaseDao
import ru.cirreth.sceniclighting.repository.dao.DeviceTypeDao

@Database(entities = [DeviceType::class, Chase::class], version=3)
@TypeConverters(LightingTypeConverter::class)
abstract class DB: RoomDatabase() {

    abstract fun deviceTypeDao(): DeviceTypeDao

    abstract fun chaseDao(): ChaseDao

}
