package ru.cirreth.sceniclighting.repository.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import ru.cirreth.sceniclighting.domain.models.DeviceType

@Dao
interface DeviceTypeDao {

    @Query("SELECT * FROM devicetype")
    fun getAll(): Flow<List<DeviceType>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg deviceType: DeviceType)

    @Update
    fun updateAll(vararg deviceType: DeviceType)

    @Delete
    fun delete(deviceType: DeviceType)

}