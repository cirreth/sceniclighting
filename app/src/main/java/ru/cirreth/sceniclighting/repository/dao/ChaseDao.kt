package ru.cirreth.sceniclighting.repository.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import ru.cirreth.sceniclighting.domain.models.Chase
import ru.cirreth.sceniclighting.domain.models.DeviceType

@Dao
interface ChaseDao {

    @Query("SELECT * FROM chase")
    fun getAll(): Flow<List<Chase>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg chase: Chase)

    @Update
    fun updateAll(vararg chase: Chase)


}