package ru.cirreth.sceniclighting.artnet

import android.util.Log
import ch.bildspur.artnet.ArtNetClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.cirreth.sceniclighting.repository.SharedPreferencesStorage

class ArtnetController(
    sharedPreferencesStorage: SharedPreferencesStorage
) {

    private val address = sharedPreferencesStorage.getArtNetAddress()
    private val artnet = ArtNetClient()
    var dmxData = UByteArray(512)

    fun connect() {
        MainScope().launch(Dispatchers.IO) {
            artnet.start()
        }
    }

    fun disconnect() {
        MainScope().launch(Dispatchers.IO) {
            artnet.stop()
        }
    }

    fun set(state: Array<UByte>) {
        MainScope().launch(Dispatchers.IO) {
            dmxData = state.clone().toUByteArray()
            artnet.unicastDmx(address.value, 0, 1, dmxData.toByteArray())
        }
    }

    fun send(channel: Int, value: Int) {
        MainScope().launch(Dispatchers.IO) {
            dmxData[channel] = value.toUByte()
            artnet.unicastDmx(address.value, 0, 1, dmxData.toByteArray())
        }
    }

}