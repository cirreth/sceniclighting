package ru.cirreth.sceniclighting.dev

import ru.cirreth.sceniclighting.domain.models.*

val deviceTypes = listOf(
    DeviceType(
        0,
        "SHEHDS LED Par 12x3W RGBW",
        Channels(
            listOf(
                Channel(0,"Общий", ChannelType.FADE),
                Channel(1,"Красный", ChannelType.FADE),
                Channel(2,"Зеленый", ChannelType.FADE),
                Channel(3,"Синий", ChannelType.FADE),
                Channel(4,"Белый", ChannelType.FADE),
                Channel(5,"Стробоскоп", ChannelType.STROBE),
                Channel(6,"Режим",
                    ChannelType.DISCRETE,
                        mapOf(
                            0 until 50 to "--",
                            51 until 100 to "Program jump",
                            101 until 150 to "Program gradient",
                            151 until 200 to "Program pulse",
                            201 until 255 to "Sound control"
                        )
                ),
                Channel(7, "Function speed", ChannelType.FADE)
            )
        )
    ),
    DeviceType(
        1,
        "INVOLIGHT MH300",
        Channels(
            listOf(
                Channel(0,"Режим",
                    ChannelType.DISCRETE,
                    mapOf(
                        0 until 24 to "RGB",
                        25 until 49 to "AUTO PRO 1",
                        50 until 74 to "AUTO PRO 2",
                        75 until 99 to "AUTO PRO 3",
                        100 until 124 to "AUTO PRO 4",
                        125 until 149 to "AUTO PRO 5",
                        150 until 174 to "AUTO PRO 6",
                        175 until 199 to "AUTO PRO 7",
                        200 until 224 to "AUTO PRO 8",
                        225 until 255 to "AUDIO PRO"
                    )
                ),
                Channel(1,"Красный", ChannelType.FADE),
                Channel(2,"Зеленый", ChannelType.FADE),
                Channel(3,"Синий", ChannelType.FADE),
                Channel(4,"Стробоскоб (в режиме RGB) / AUTO SPEED", ChannelType.FADE),
                Channel(5,"Диммер", ChannelType.FADE_REVERSE),
                Channel(6,"PAN", ChannelType.PAN), // 540 deg on 255
                Channel(7,"TILT", ChannelType.TILT), // 230 deg on 255
                Channel(8,"Скорость вращения", ChannelType.FADE_REVERSE),
                Channel(9,"Подстройка PAN", ChannelType.FADE), // 2.4 deg on 255
                Channel(10, "Подстройка TILT", ChannelType.FADE), // 2.4 deg on 255
            ),
        )
    ),
    DeviceType(
        2,
        "SHEHDS Bee Eye 6x15W",
        Channels(
            listOf(
                Channel(0,"PAN", ChannelType.PAN),
                Channel(1,"TILT", ChannelType.TILT),
                Channel(2,"ZOOM?", ChannelType.FADE),
                /*
                Channel("Режим",
                    ChannelType.DISCRETE,
                    mapOf(
                        0 until 127 to "Positive offset",
                        128 until 191 to "Positive rotation",
                        192 until 255 to "Reverse rotation"
                    )
                ),
                 */
                Channel(3,"Диммер", ChannelType.FADE),
                Channel(4,"Красный", ChannelType.FADE),
                Channel(5,"Зеленый", ChannelType.FADE),
                Channel(6,"Синий", ChannelType.FADE),
                Channel(7,"Белый", ChannelType.FADE),
                Channel(8,"Стробоскоб", ChannelType.FADE),
                Channel(9,"Режим",
                    ChannelType.DISCRETE,
                    mapOf(
                        0 until 10 to "Other channels",
                        11 until 127 to "Combination of color change",
                        128 until 160 to "Color jump",
                        161 until 200 to "Color gradient 1",
                        201 until 255 to "Color gradient 2"
                    )
                ),
                Channel(10, "CH10 speed regulation", ChannelType.FADE),
            )
        ),
        presets = Presets(
            listOf(
                Preset("white beam",
                    listOf(
                        ChannelSet(2, 20.toUByte()),
                        ChannelSet(3, 255.toUByte()),
                        ChannelSet(4, 0.toUByte()),
                        ChannelSet(5, 0.toUByte()),
                        ChannelSet(6, 0.toUByte()),
                        ChannelSet(7, 255.toUByte()),
                        ChannelSet(8, 0.toUByte()),
                        ChannelSet(9, 0.toUByte()),
                    )
                ),
                Preset("rotation start",
                    listOf(
                        ChannelSet(0, 0.toUByte()),
                        ChannelSet(1, 128.toUByte()),
                        ChannelSet(2, 131.toUByte()),
                        ChannelSet(3, 255.toUByte()),
                        ChannelSet(4, 0.toUByte()),
                        ChannelSet(5, 0.toUByte()),
                        ChannelSet(6, 0.toUByte()),
                        ChannelSet(7, 255.toUByte()),
                        ChannelSet(8, 0.toUByte()),
                        ChannelSet(9, 0.toUByte()),
                    )
                )
            )
        )
    ),
    DeviceType(
        3,
        "Alien RGB Laser",
        Channels(
            listOf(
                Channel(0,"Режим",
                    ChannelType.DISCRETE,
                    mapOf(
                        0 until 63 to "Closed light",
                        64 until 127 to "Manual",
                        128 until 191 to "Auto",
                        192 until 255 to "Sound",
                    )
                ),
                Channel(1,"Pattern/Auto mode", ChannelType.FADE),
                Channel(2,"Angle control", ChannelType.FADE),
                Channel(3,"Horizontal angle", ChannelType.FADE),
                Channel(4,"Vertical angle", ChannelType.FADE),
                Channel(5,"Horizontal position", ChannelType.FADE),
                Channel(6,"Vertical position", ChannelType.FADE),
                Channel(7, "Function speed", ChannelType.FADE),
                Channel(8, "Color control", ChannelType.FADE),
                Channel(9, "Code control", ChannelType.FADE),
            )
        )
    ),
)

val devices = listOf(
    Device(0, 0, deviceTypes[0]),
    Device(1, 15, deviceTypes[1]),
    Device(2, 31, deviceTypes[2]),
    Device(3, 47, deviceTypes[3])
)

val arrangement = Arrangement(
    0, 3, 3, 3,
    mapOf(
        SpaceCoord(5, 10, 0) to devices[0],
        SpaceCoord(10, 10, 0) to devices[1],
        SpaceCoord(3, 10, 0) to devices[2],
        SpaceCoord(10, 10, 0) to devices[3]
    )
)


