package ru.cirreth.sceniclighting.domain.models

enum class ChannelType(type: Int) {
    FADE(0),
    PAN(1),
    TILT(2),
    STROBE(3),
    DISCRETE(4),
    FADE_REVERSE(5)
}
