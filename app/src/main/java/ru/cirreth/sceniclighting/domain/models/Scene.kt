package ru.cirreth.sceniclighting.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Scene(
    val changeTime: Long,
    val time: Long,
    val values: Array<UByte> = Array(512, init = { 0.toUByte() })
)

@Entity
data class Chase(
    @PrimaryKey
    val id: Long = 0,
    val frames: List<Scene> = mutableListOf()
)
