package ru.cirreth.sceniclighting.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DeviceType(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var name: String,
    var channels: Channels,
    var presets: Presets = Presets(listOf())
)

data class Presets(
    val presets: List<Preset>
)

data class Preset(
    val name: String,
    val channelSets: List<ChannelSet>
)

data class ChannelSet(
    val channel: Int,
    val value: UByte
)

data class Device(
    var id: Int,
    var startChannel: Int,
    var deviceType: DeviceType
)
