package ru.cirreth.sceniclighting.domain.models

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

@ProvidedTypeConverter
class LightingTypeConverter(private val gson: Gson) {

    @TypeConverter
    fun fromChannels(channels: Channels): String {
        return gson.toJson(channels)
    }

    @TypeConverter
    fun toChannels(json: String): Channels {
        return gson.fromJson(json, Channels::class.java)
    }

    @TypeConverter
    fun fromPresets(presets: Presets): String {
        return gson.toJson(presets)
    }

    @TypeConverter
    fun toPresets(json: String): Presets {
        return gson.fromJson(json, Presets::class.java)
    }

    @TypeConverter
    fun toChase(json: String): Chase {
        return gson.fromJson(json, Chase::class.java)
    }

    @TypeConverter
    fun fromChase(chase: Chase): String {
        return gson.toJson(chase)
    }

    @TypeConverter
    fun toScene(json: String): Scene {
        return gson.fromJson(json, Scene::class.java)
    }

    @TypeConverter
    fun fromScene(scene: Scene): String {
        return gson.toJson(scene)
    }

    @TypeConverter
    fun toFrames(json: String): List<Scene> {
        val type: Type = object : TypeToken<List<Scene>>() {}.type
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun fromFrames(frames: List<Scene>): String {
        val type: Type = object : TypeToken<List<Scene>>() {}.type
        return gson.toJson(frames, type)
    }

}
