package ru.cirreth.sceniclighting.domain.models

data class Channels(
    val channels: List<Channel>
)

data class Channel(
    val number: Int,
    val name: String,
    val type: ChannelType,
    val discreteRanges: Map<IntRange, String>? = null,
    val minValue: Int = 0,
    val maxValue: Int = 255
)
