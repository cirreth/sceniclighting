package ru.cirreth.sceniclighting.domain.models

data class SettingsData(
    val address: String
)
