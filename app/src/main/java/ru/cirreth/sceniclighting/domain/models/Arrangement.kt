package ru.cirreth.sceniclighting.domain.models

import androidx.room.Entity

@Entity
data class Arrangement(
    var id: Int,
    var width: Int,
    var depth: Int,
    var height: Int,
    var devices: Map<SpaceCoord, Device>
)

data class SpaceCoord(
    var left: Int,
    var depth: Int,
    var height: Int
)
